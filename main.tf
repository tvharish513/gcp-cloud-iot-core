provider "google" {
  credentials = file("../smart-pride-353020-28d52ffc0671.json")
}

resource "google_pubsub_topic" "default-devicestatus" {
  name    = "default-devicestatus"
  project = "smart-pride-353020"
}

resource "google_pubsub_topic" "default-telemetry" {
  name    = "default-telemetry"
  project = "smart-pride-353020"
}

resource "google_pubsub_topic" "additional-telemetry" {
  name    = "additional-telemetry"
  project = "smart-pride-353020"
}

resource "google_cloudiot_registry" "test-registry" {
  name    = "cloudiot-registry"
  project = "smart-pride-353020"
  region  = "us-central1"

  event_notification_configs {
    pubsub_topic_name = google_pubsub_topic.additional-telemetry.id
    subfolder_matches = "test/path"
  }

  event_notification_configs {
    pubsub_topic_name = google_pubsub_topic.default-telemetry.id
    subfolder_matches = ""
  }

  state_notification_config = {
    pubsub_topic_name = google_pubsub_topic.default-devicestatus.id
  }

  mqtt_config = {
    mqtt_enabled_state = "MQTT_ENABLED"
  }

  http_config = {
    http_enabled_state = "HTTP_ENABLED"
  }

  log_level = "INFO"

  credentials {
    public_key_certificate = {
      format      = "X509_CERTIFICATE_PEM"
      certificate = file("../certs/ca_cert_registry.pem")
    }
  }
}

resource "google_cloudiot_device" "test-device" {
  name     = "cloudiot-device"
  registry = google_cloudiot_registry.test-registry.id

  credentials {
    public_key {
      format = "ES256_X509_PEM"
      key    = file("../certs/ec_cert_device1.pem")
    }
  }

  blocked = false

  log_level = "INFO"

  metadata = {
    test_key_1 = "test_value_1"
  }

  gateway_config {
    gateway_type = "NON_GATEWAY"
  }
}